# README #



### What is this repository for? ###

* Addon to provide some infomration into th ecurrent state of all repos the user has access to.



### Dependancies ###

* npm (npm install -g bower)

### How do I get set up? ###

* Clone the repo
* grab an auth consumer key from UserProfile manakin -> 'Bitbucket Settings' -> left panel 'Assess Management' -> Oauth
* replace **YOURKEY** in connect.json with consumer key
* cd js; bower install
* via UserProfile manakin -> Integrations, add the addon via url https://mws-30995.mws3.csx.cam.ac.uk/projectprogress/connect.json

### Develop code

#### Windows - using docker

```
clone < this project >
cd < this project >

docker run -dit --name my-apache-app -v <thi sproject dir>:/usr/local/apache2/htdocs/ httpd:2.4

docker run --rm -it --link my-apache-app wernight/ngrok ngrok http my-apache-app

```

Then in bitbucket register the addon with the temp development URL